const express = require("express");
const _ = require("lodash");
const AppConst = require("../api.endpoints");
const bookModel = require("../schemas/books.schema");
const router = express.Router();

// /* Saving Document Details */
router.post(AppConst.BOOK, (req, res) => {
  const newBook = new bookModel(req.body);
  newBook.save((err, result) => {
    if (err) {
      res.status(500).json({
        err,
      });
    } else if (!_.isEmpty(result)) {
      res.status(200).json(result);
    }
  });
});

// /* Update Document Details */
router.put(AppConst.BOOK, (req, res) => {
  bookModel.findByIdAndUpdate(req.query.id, req.body, (err, result) => {
    if (err) {
      res.status(500).json({
        err,
      });
    } else if (!_.isEmpty(result)) {
      res.status(200).json(result);
    }
  });
});

// /* GET Document detail listing. */
router.get(AppConst.BOOK, (req, res) => {
  // req.query.userId = req.currentUser.mobileNo;
  bookModel.find(req.query, (err, data) => {
    if (err) {
      res.status(400).json({
        err,
      });
    } else {
      res.status(200).json({
        data,
      });
    }
  });
});

// /* Delete Document */
router.delete(AppConst.BOOK, (req, res) => {
  bookModel.findByIdAndDelete(req.query.id, (err, data) => {
    if (err) {
      res.status(400).send("data found error");
    }
    res.status(200).send(data);
  });
});

module.exports = router;
