const express = require("express");
const router = express.Router();
const jwt = require("jsonwebtoken");
const _ = require("lodash");

const UserModel = require("../schemas/user.schema");
const AppConst = require("../api.endpoints");

const accessTokenSecret = "youraccesstokensecret";

/**
 * !Route to Check is User Login?
 */
router.get(AppConst.IS_LOGIN, (req, res) => {
  
});

/* GET users listing. */
router.get(AppConst.ALLUSER, (req, res) => {
  UserModel.find({}, (err, data) => {
    if (err) {
      res.status(400).send("data found error");
    }
    res.status(200).send(data);
  });
});

/* Get Usr by id */
router.get(AppConst.USER, (req, res) => {
  req.body.loginId = req.currentUser.mobileNo;
  UserModel.find(req.query).exec((err, user) => {
    if (err) {
      res.status(500).send(err);
    } else {
      res.send(user);
    }
  });
});

/* User Registration */
router.post(AppConst.SIGNUP, async (req, res) => {
  try {
    const user = new UserModel(req.body);
    const result = await user.save();
    res.status(200).json({
      result
    });
  } catch (error) {
    res.status(500).json({error});
  }
});

/* User Signin */
router.post(AppConst.LOGIN, (req, res) => {
  if (!req.body.mobileNo || !req.body.password) {
    res.status(400).json({
      message: "Please Enter Valid Credentials",
    });
  } else {
    UserModel.find(req.body, (err, data) => {
      if (err) {
        res.status(400).json({
          message : "Credentials Not Valid"
        });
      } else if (!_.isEmpty(data)) {
        const accessToken = jwt.sign(
          {
            subject: data[0]._id,
          },
          accessTokenSecret,
          { expiresIn: '86400s' }
        );
        res.status(200).json({
          accessToken,
        });
      } else {
        res.status(400).json({
          message : "Credentials Not Valid"
        });
      }
    });
  }
});

router.get("/logout", (req, res) => {
  req.session.destroy(() => {
    console.log("user logged out.");
  });
  res.status(200).send("logout Success");
  // res.redirect('/login');
});

module.exports = router;
