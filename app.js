var createError = require("http-errors");
var express = require("express");
var path = require("path");
const bodyParser = require("body-parser");
var cookieParser = require("cookie-parser");
var logger = require("morgan");
const mongoose = require("mongoose");
var debug = require("debug")("vue-sample-server:server");
var http = require("http");
var cors = require("cors");
var app = express();

/**
 * ! Importing Routes
 */
var bookRouter = require("./routes/book.route");
var userRouter = require("./routes/users.route");
var indexRouter = require("./routes/index.route");

/**
 * ! Importing Middelwares
 */
var verifyToken = require("./middlewares/jwt.verifier");

app.use(logger("dev"));
app.use(bodyParser.json());
app.use(
  bodyParser.urlencoded({
    extended: true,
  })
);
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, "public")));
app.use(cors());

/**
 * ! Using Imported Routes
 */
app.all("*", verifyToken);
app.use(bookRouter);
app.use("/api/", indexRouter);
app.use(userRouter);

/**
 * Get port from environment and store in Express.
 */

var port = process.env.PORT || "3001";
app.set("port", port);

/**
 * Create HTTP server.
 */

var server = http.createServer(app);
server.listen(port);
/**
 * Event listener for HTTP server "error" event.
 */
server.on("error", (error) => {
  if (error.syscall !== "listen") {
    throw error;
  }

  var bind = typeof port === "string" ? "Pipe " + port : "Port " + port;

  // handle specific listen errors with friendly messages
  switch (error.code) {
    case "EACCES":
      console.error(bind + " requires elevated privileges");
      process.exit(1);
      break;
    case "EADDRINUSE":
      console.error(bind + " is already in use");
      process.exit(1);
      break;
    default:
      throw error;
  }
});

/**
 * Event listener for HTTP server "listening" event.
 */

server.on("listening", () => {
  var addr = server.address();
  var bind = typeof addr === "string" ? "pipe " + addr : "port " + addr.port;
  debug("Listening on " + bind);
  console.log(`Listening on ${bind}`);
});

/**
 * !Connecting to database server on  https://cloud.mongodb.com/
 */
mongoose.connect(
  "mongodb+srv://bharat:Bharat%4028@cluster0-ixbyo.mongodb.net/vue-sample-db?retryWrites=true&w=majority",
  {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useFindAndModify: false,
    useCreateIndex: true,
  }
);
const db = mongoose.connection;
db.on("error", console.error.bind(console, "connection error:"));
db.once("open", () => {
  console.log("Connected to Db");
});

module.exports = app;
