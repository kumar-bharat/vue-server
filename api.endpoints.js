module.exports = {
  SIGNUP: "/api/register",
  LOGIN: "/api/login",
  IS_LOGIN: "/api/isLogin",
  LOGOUT: "/api/logout",
  USER: "/api/user",
  ALLUSER: "/api/alluser",
  BOOK: "/api/books",
};
