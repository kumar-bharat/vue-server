const mongo = require("mongoose");

const { Schema } = mongo;

const UsersSchema = new Schema({
  userName: {
    type: String,
    required: true,
  },
  mobileNo: {
    type: Number,
    required: true,
    unique: true,
  },
  password: {
    type: String,
    required: true,
  },
});

const UserModel = mongo.model("users", UsersSchema);

module.exports = UserModel;
