/* eslint-disable linebreak-style */
const mongo = require("mongoose");

const { Schema } = mongo;

const bookSchema = new Schema({
  // userId: {
  //   type: String,
  //   required: true,
  // },
  bookName: {
    type: String,
    required: true,
  },
  authorName: {
    type: String,
  },
});

const bookModel = mongo.model("books", bookSchema);

module.exports = bookModel;
