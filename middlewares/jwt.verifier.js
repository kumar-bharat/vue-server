const _ = require("lodash");
const jwt = require("jsonwebtoken");
const UserModel = require("../schemas/user.schema");
const AppConst = require("../api.endpoints");

const accessTokenSecret = "youraccesstokensecret";
const excludePaths = [AppConst.LOGIN, AppConst.SIGNUP, AppConst.ALLUSER];
module.exports = (req, res, next) => {
  if (_.includes(excludePaths, req.path)) {
    return next();
  }
  const requestToken = req.headers.authorization;
  let token = requestToken ? requestToken.split(" ")[1] : null;
  token = token === "null" ? null : token;
  if (token === null) {
    res.status(401).json({
      message: "Authentication Failed",
    });
  }
  jwt.verify(token, accessTokenSecret, (err, payload) => {
    if (!payload) {
      res.status(401).json({
        message: "Authentication Failed",
      });
    }
    UserModel.findById(payload.subject, (err, data) => {
      if (err) {
        res.status(401).json({
          message: "Authentication Failed",
        });
      } else if (data) {
        req.currentUser = data._doc;
        return next();
      }
    });
  });
};
